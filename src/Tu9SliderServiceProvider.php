<?php
// MyVendor\contactform\src\TU9ServiceProvider.php
namespace Tu9\Tu9Slider;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Tu9\Tu9Slider\View\Components\Tu9Slider;

class Tu9SliderServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'tu9-slider');
        Blade::component('tu9-slider', Tu9slider::class);
    }

    public function register()
    {
    }
}
